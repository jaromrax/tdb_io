#!/usr/bin/env python3

from fire import Fire
import tdb_io.influx as influx

def main( db, meas ):
    print()
    if influx.check_port() :
        print("i... influx port present localy (check_port)")

    commavalues="a=1"
    print("i... BRUTAL WRITE to 127.0.0.1 : ")
    influx.influxwrite( DATABASE=db, MEASUREMENT=meas, values=commavalues, IP="127.0.0.1" , DEBUG=True)
    #    influx.influxwrite( DATABASE="local", MEASUREMENT="flashcam", values=commavalues, IP="127.0.0.1" )

if __name__=="__main__":
    Fire(main)
