import sys
sys.path.append(".")   # can find version in . by pytest
sys.path.append("./tdb_io")   # can find version from Jupyter (above)
from tdb_io.version import __version__
try:
    from tdb_io.version import __version__
except:
    print()
