#!/usr/bin/env python3

from fire import Fire
from os import listdir
from os.path import isfile, join
import glob
import os
import subprocess as sp


def chk_influx():
    print("D... check influx ssl file's directory")
    with open("/etc/influxdb/influxdb.conf") as f:
        res = f.readlines()
    res = [ x for x in res if x.find("#")<0 ]
    res = [ x.strip() for x in res if x.find("ssl")>0 ]
    res = [ x.split("=")[1].strip().strip('"')  for x in res ]

    print("D... influx needs these two:",res)
    full = [x for x in res if x.find("fullchain.pem")>0]
    pkey = [x for x in res if x.find("privkey.pem")>0]

    return full[0],pkey[0]

def main():
    fullchain,pkey = chk_influx()

    print("D... main update keys for grafana reading influx")
    print(" ... i assume that grafana has own certificate (due to domainname grafana.)  ")
    print(" ... i assume influx is accessed on www. ")
    certpath = "/etc/letsencrypt/archive"
    dirs = glob.glob(certpath+"/*/")
    print("D... dirs seen:",dirs)
    for i in dirs:
        #print("D..>",i[0],i[-1], i[-2])
        #============== WWW.CZ================
        if (i.find("www.")>=0) and (i[-2]=="z"):
            #wdir = certpath+"/"+i
            print(f"D... globbing {i}")

            files = glob.glob( i+"fullchain*" )
            #print( files )
            latest_fullchain = max(files, key=os.path.getctime)
            print(latest_fullchain)

            files = glob.glob( i+"privkey*" )
            #print( files )
            latest_privkey = max(files, key=os.path.getctime)
            print(latest_privkey)

            # execute ========================
            CMD1 = f"cp {latest_fullchain} {fullchain}"
            CMD2 = f"cp {latest_privkey} {pkey}"
            print(CMD1)
            print(CMD2)
            status = sp.call( CMD1 , shell=True)
            status = sp.call( CMD2 , shell=True)

            CMD = "systemctl restart influxdb.service"
            print(CMD)
            status = sp.call( CMD , shell=True)
            print(status)



if __name__=="__main__":
    Fire(main)
