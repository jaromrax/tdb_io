#!/usr/bin/env python3
"""
 i insert an image to mongo. Put labels to see that all is correct.
 check sizes with:
mongo
show dbs
quit()
"""


import pymongo
import io
from bson.binary import Binary
import sys
import datetime
from PIL import Image, ImageDraw, ImageFont



#========================== DATE TAG
datum =datetime.datetime.now().strftime("%H:%M:%S")
print("D... now==",datum)

#========================== JPG OPEN

image = Image.open('/home/ojr/Pictures/mpv-shot0035.jpg')
width, height = image.size
print("D... wxh", image.size )
text = "Entering mongo "+datum

draw = ImageDraw.Draw(image)  # DARW OBLEJT
textwidth, textheight = draw.textsize(text)
margin = 30
x = width - textwidth - margin
y = height - textheight - margin
draw.text((x, y), text)

image.show()

#=========================== MONGO

client = pymongo.MongoClient("localhost", 27017)

db = client.test
print(db.name)
print(db.test01)



res = db.test01.find()
for i in res:
    if 'a' in i:
        print("D... ", i['a'] )
res = db.test01.find( {"a":"18:28:30"} )
for i in res:
    print("found a,w,h=",i['a'], i['w'], i['h'] )
    # image2 = Image.open( i["image"] )
    # image2 = Image.open(io.BytesIO( i["image"] ))
    # JPG is RGB
    image2 = Image.frombytes('RGB', (i['w'],i['h']), i["image"] , 'raw')
    text = "Extracted from mongo "+ datum

    draw = ImageDraw.Draw(image2)  # DARW OBLEJT
    textwidth, textheight = draw.textsize(text)
    margin = 40

    x =   margin
    y = margin
    draw.text((x, y), text)
    image2.show()


# sys.exit(0)

image_data = Binary(image.tobytes())

dato={
    "a": datum,
    "w": image.size[0],
    "h": image.size[1],

    "image":image_data
}

print( db.test01.insert_one( dato ) )
print("DONE")
#Binary_image_file = Binary(image_file) #pymongo libary
# image = Image.open(image_data)
